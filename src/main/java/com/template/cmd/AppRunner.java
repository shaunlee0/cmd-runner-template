package com.template.cmd;

import com.template.cmd.services.HealthService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class AppRunner implements CommandLineRunner {

    private final HealthService healthService;

    @Autowired
    public AppRunner(HealthService healthService) {
        this.healthService = healthService;
    }

    @Override
    public void run(String... args) throws Exception {
       log.info("Started the AppRunner...");
       log.info("Health response: {}", healthService.getHealthResponse().getMessage());
    }
}
