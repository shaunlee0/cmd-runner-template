Command Line Runner Template
==============================

Overview
--------

This project is built using:

- Spring Framework
- Maven

Getting Started
---------------


### Maven wrapper

If you do not have maven installed a maven wrapper is supplied for Mac OS & PC, substitute ```mvn``` for either ```./mvnw``` on a Mac or ```mvnw.cmd``` on a PC


### Starting the application

You can run the integration and unit tests Maven:

```
mvn clean test
```

### Starting the application

You can start the application using Maven:

```
mvn spring-boot:run
```
