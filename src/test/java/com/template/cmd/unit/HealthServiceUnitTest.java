package com.template.cmd.unit;

import com.template.cmd.dto.APIResponse;
import com.template.cmd.services.HealthService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HealthServiceUnitTest {

    @InjectMocks
    private HealthService healthService;

    private static final String SUCCESS_MESSAGE = "success";

    @Test
    public void shouldGetHealthResponse() {

        APIResponse healthResponse = healthService.getHealthResponse();

        MatcherAssert.assertThat(healthResponse,
                Matchers.hasProperty("message",is(SUCCESS_MESSAGE))
        );

    }
}
