package com.template.cmd.services;

import com.template.cmd.dto.APIResponse;
import org.springframework.stereotype.Service;

@Service
public class HealthService {

    private static final String SUCCESS_MESSAGE = "success";

    public APIResponse getHealthResponse() {
        return new APIResponse(SUCCESS_MESSAGE);
    }
}
