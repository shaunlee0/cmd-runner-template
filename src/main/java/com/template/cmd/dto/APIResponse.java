package com.template.cmd.dto;

import lombok.Data;

@Data
public class APIResponse {
    private String message;

    public APIResponse(String message) {
        this.message = message;
    }
}
