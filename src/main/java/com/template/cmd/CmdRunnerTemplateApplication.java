package com.template.cmd;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Log4j2
public class CmdRunnerTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(CmdRunnerTemplateApplication.class, args).close();
    }

}
